package com.lepaya.myapplication

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import com.lepaya.myapplication.business.nearbyFoodVenues.FoodVenuesModel
import com.lepaya.myapplication.business.nearbyFoodVenues.NearbyFoodVenuesVM
import com.lepaya.myapplication.business.nearbyFoodVenues.NearbyVenuesRepository
import com.lepaya.myapplication.location.GeoLocation
import com.lepaya.myapplication.location.InValidLocationException
import com.lepaya.myapplication.location.LocationRetriever
import com.lepaya.myapplication.source.remote.networkclient.ErrorResponse
import com.lepaya.myapplication.source.remote.networkclient.Meta
import com.mahmoudibra.mvvmtemplate.source.remote.Result
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException

class NearbyFoodVenuesVMTest {

    // Dependence
    private var nearbyVenuesRepository: NearbyVenuesRepository = mockk()
    private var locationRetriever: LocationRetriever = mockk()

    // ViewModel
    private lateinit var nearbyFoodVenuesVM: NearbyFoodVenuesVM


    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()


    @Before
    fun setupViewModel() {
        nearbyFoodVenuesVM =
            NearbyFoodVenuesVM(
                nearbyVenuesRepository = nearbyVenuesRepository,
                locationRetriever = locationRetriever
            )
    }

    @Test
    fun `Test that fetch customer location will return location data successfully`() {
        // Mocked data
        val mockedLocation = GeoLocation(13.00, 51.00)
        val foodVenousList = listOf(
            FoodVenuesModel(url = "http://12345"),
        )
        val nearbyVenousResponse = Result.success(foodVenousList)

        // Stub
        coEvery { locationRetriever.getLastLocation() } answers { mockedLocation }
        coEvery { nearbyVenuesRepository.fetchRepositories(latLng = mockedLocation.getLatLngCompined()) } answers { nearbyVenousResponse }

        // Action
        nearbyFoodVenuesVM.getCustomerLocationAndNearbyFoodVenous()

        // Assert
        nearbyFoodVenuesVM.viewState.value?.userLocation?.apply {
            Truth.assertThat(this)
                .isEqualTo(mockedLocation)
        }

        Truth.assertThat(nearbyFoodVenuesVM.inProgress.value).isEqualTo(false)

        // Verify
        coVerify(exactly = 1) { locationRetriever.getLastLocation() }
    }

    @Test
    fun `Test that when fetch customer location throws error it should be handled successfully`() {
        // Mocked data
        val mockedLocation = GeoLocation(13.00, 51.00)
        val foodVenousList = listOf(
            FoodVenuesModel(url = "http://12345"),
        )
        val nearbyVenousResponse = Result.success(foodVenousList)

        // Stub
        coEvery { locationRetriever.getLastLocation() } throws InValidLocationException()
        coEvery { nearbyVenuesRepository.fetchRepositories(latLng = mockedLocation.getLatLngCompined()) } answers { nearbyVenousResponse }

        // Action
        nearbyFoodVenuesVM.getCustomerLocationAndNearbyFoodVenous()

        // Assert
        nearbyFoodVenuesVM.viewState.value?.error?.apply {
            Truth.assertThat(this)
                .isEqualTo(InValidLocationException())
        }

        Truth.assertThat(nearbyFoodVenuesVM.inProgress.value).isEqualTo(false)

        // Verify
        coVerify(exactly = 1) { locationRetriever.getLastLocation() }
        coVerify(exactly = 0) {nearbyVenuesRepository.fetchRepositories(latLng = any()) }
    }



    @Test
    fun `Test that fetch nearby location will return successfully`() {
        // Mocked data
        val mockedLocation = GeoLocation(13.00, 51.00)
        val foodVenousList = listOf(
            FoodVenuesModel(url = "http://12345"),
            FoodVenuesModel(url = "http://12345"),
            FoodVenuesModel(url = "http://12345"),
        )
        val nearbyVenousResponse = Result.success(foodVenousList)

        // Stub
        coEvery { locationRetriever.getLastLocation() } answers { mockedLocation }
        coEvery { nearbyVenuesRepository.fetchRepositories(latLng = mockedLocation.getLatLngCompined()) } answers { nearbyVenousResponse }

        // Action
        nearbyFoodVenuesVM.getCustomerLocationAndNearbyFoodVenous()

        // Assert
        nearbyFoodVenuesVM.viewState.value?.userLocation?.apply {
            Truth.assertThat(this)
                .isEqualTo(mockedLocation)
        }

        nearbyFoodVenuesVM.viewState.value?.nearbyFoodVenuesList?.apply {
            Truth.assertThat(this)
                .isEqualTo(foodVenousList)
        }

        Truth.assertThat(nearbyFoodVenuesVM.inProgress.value).isEqualTo(false)

        // Verify
        coVerify(exactly = 1) { locationRetriever.getLastLocation() }
        coVerify(exactly = 1) { nearbyVenuesRepository.fetchRepositories(latLng = mockedLocation.getLatLngCompined()) }
    }


    @Test
    fun `Test that when fetch nearby food venious throws exception will be handled successfully`() {
        // Mocked data
        val mockedLocation = GeoLocation(13.00, 51.00)

        val nearbyVenousResponse = Result.error(exception = IOException())

        // Stub
        coEvery { locationRetriever.getLastLocation() } answers { mockedLocation }
        coEvery { nearbyVenuesRepository.fetchRepositories(latLng = mockedLocation.getLatLngCompined()) } answers { nearbyVenousResponse }

        // Action
        nearbyFoodVenuesVM.getCustomerLocationAndNearbyFoodVenous()

        // Assert
        nearbyFoodVenuesVM.viewState.value?.error?.apply {
            Truth.assertThat(this)
                .isEqualTo(IOException())
        }

        Truth.assertThat(nearbyFoodVenuesVM.inProgress.value).isEqualTo(false)

        // Verify
        coVerify(exactly = 1) { locationRetriever.getLastLocation() }
        coVerify(exactly = 1) { nearbyVenuesRepository.fetchRepositories(latLng = mockedLocation.getLatLngCompined()) }
    }

    @Test
    fun `Test that when fetch nearby food venious throws error response`() {
        // Mocked data
        val mockedLocation = GeoLocation(13.00, 51.00)

        val nearbyVenousResponse = Result.systemError(error = ErrorResponse(Meta(errorDetail = "UnValid any thing")))

        // Stub
        coEvery { locationRetriever.getLastLocation() } answers { mockedLocation }
        coEvery { nearbyVenuesRepository.fetchRepositories(latLng = mockedLocation.getLatLngCompined()) } answers { nearbyVenousResponse }

        // Action
        nearbyFoodVenuesVM.getCustomerLocationAndNearbyFoodVenous()

        // Assert
        nearbyFoodVenuesVM.viewState.value?.errorMessage?.apply {
            Truth.assertThat(this)
                .isEqualTo("UnValid any thing")
        }

        Truth.assertThat(nearbyFoodVenuesVM.inProgress.value).isEqualTo(false)

        // Verify
        coVerify(exactly = 1) { locationRetriever.getLastLocation() }
        coVerify(exactly = 1) { nearbyVenuesRepository.fetchRepositories(latLng = mockedLocation.getLatLngCompined()) }
    }


    @Test
    fun `Test that handle location activity result handled successfully`() {
        // Stub
        every { locationRetriever.handleActivityResult(100, 200) } answers {}

        // Action
        nearbyFoodVenuesVM.handleLocationActivityResult(100,200)

        // Verify
        verify(exactly = 1) { locationRetriever.handleActivityResult(100, 200) }
    }


    @Test
    fun `Test that stop location retriever handled successfully`() {
        // Stub
        every { locationRetriever.stopLocationUpdates() } answers {}

        // Action
        nearbyFoodVenuesVM.stopLocationUpdates()

        // Verify
        verify(exactly = 1) { locationRetriever.stopLocationUpdates() }
    }
}