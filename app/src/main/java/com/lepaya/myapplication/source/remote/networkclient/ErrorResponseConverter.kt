package com.lepaya.myapplication.source.remote.networkclient

import okhttp3.ResponseBody
import java.io.IOException

object ErrorResponseConverter {
    fun parseError(errorBody: ResponseBody): ErrorResponse = try {
        NetworkApiClient.apiClient.responseBodyConverter<ErrorResponse>(
            ErrorResponse::class.java,
            arrayOfNulls(0)
        ).convert(errorBody)
            ?: ErrorResponse()
    } catch (e: IOException) {
        ErrorResponse()
    }
}

data class ErrorResponse(
    var meta: Meta = Meta(),
)

data class Meta(
    var code: Int = 0,
    var errorDetail: String = "",
    var errorType: String = "",
    var requestId: String = ""
)