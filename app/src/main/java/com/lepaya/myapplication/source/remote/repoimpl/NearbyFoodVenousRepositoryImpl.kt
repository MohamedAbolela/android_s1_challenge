package com.lepaya.myapplication.source.remote.repoimpl

import com.lepaya.myapplication.business.nearbyFoodVenues.NearbyVenuesRepository
import com.lepaya.myapplication.business.nearbyFoodVenues.FoodVenuesModel
import com.mahmoudibra.mvvmtemplate.source.remote.Result
import com.lepaya.myapplication.source.remote.endpoint.FoodVenuesEndpoints
import com.lepaya.myapplication.source.remote.networkclient.NetworkApiClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NearbyFoodVenousRepositoryImpl : NearbyVenuesRepository {

    private val trendingEndpoint: FoodVenuesEndpoints by lazy {
        NetworkApiClient.apiClient.create(FoodVenuesEndpoints::class.java)
    }

    override suspend fun fetchRepositories(latLng: String): Result<List<FoodVenuesModel>> {
        return request {
            withContext(Dispatchers.IO) {
                trendingEndpoint.fetchNearbyFoodVenous(latLng = latLng)
            }
        }
    }
}
