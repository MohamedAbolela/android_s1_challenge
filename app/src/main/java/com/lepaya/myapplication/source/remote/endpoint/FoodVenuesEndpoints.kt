package com.lepaya.myapplication.source.remote.endpoint

import com.lepaya.myapplication.business.nearbyFoodVenues.FoodVenuesModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FoodVenuesEndpoints {
    @GET("/v2/venues/search")
    suspend fun fetchNearbyFoodVenous(
        @Query("ll") latLng: String
    ): Response<List<FoodVenuesModel>>
}
