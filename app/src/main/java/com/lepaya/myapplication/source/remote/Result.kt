package com.mahmoudibra.mvvmtemplate.source.remote

import com.lepaya.myapplication.source.remote.networkclient.ErrorResponse

sealed class Result<out T> {

    data class Success<out T>(val data: T) : Result<T>()
    data class SystemError(val error: ErrorResponse) : Result<Nothing>()
    data class Error(val exception: Exception) : Result<Nothing>()

    companion object {
        fun <T> success(data: T): Result<T> =
            Success(data)

        fun systemError(error: ErrorResponse): Result<Nothing> =
            SystemError(error)

        fun error(exception: Exception): Result<Nothing> =
            Error(exception)
    }
}
