package com.lepaya.myapplication.base

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.lepaya.myapplication.business.common.ProgressDialog
import com.lepaya.myapplication.extention.toast.showErrorMessage
import com.lepaya.myapplication.extention.toast.showSuccessMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

abstract class BaseActivity<P : BaseVM, V : ViewDataBinding> : AppCompatActivity(),
    CoroutineScope {

    private lateinit var mJob: Job
    override val coroutineContext: CoroutineContext get() = mJob + Dispatchers.Main
    private var progressDialog: ProgressDialog? = null

    lateinit var presentationModel: P

    val dataBindingView by lazy {
        DataBindingUtil.setContentView(this, getLayoutId()) as V
    }

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initialize()
        observeLiveData()
    }

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun initialize()

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        onBackPressed()
        return true
    }

    private fun observeLiveData() {
        presentationModel.showSuccessMessage.observe(this, {
            Timber.e("Show Success Message")
            Toast(applicationContext).showSuccessMessage(
                applicationContext,
                it
            )
        })

        presentationModel.showErrorMessage.observe(this, {
            Timber.e("Show Error Message")
            Toast(applicationContext).showErrorMessage(
                applicationContext,
                it
            )

        })
        presentationModel.inProgress.observe(this, Observer {
            when (it) {
                true -> showProgressDialog()
                false -> hideProgressDialog()
            }
        })
    }

    private fun showProgressDialog() {
        progressDialog?.hideProgressDialog()
        progressDialog?.showProgressDialog()
    }

    private fun hideProgressDialog() {
        progressDialog?.hideProgressDialog()
    }


    override fun onResume() {
        super.onResume()
        presentationModel.hydrate()
    }
}
