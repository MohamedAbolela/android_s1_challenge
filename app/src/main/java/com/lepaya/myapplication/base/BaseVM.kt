package com.lepaya.myapplication.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseVM : ViewModel() {

    protected val _inProgress = MutableLiveData<Boolean>()
    val inProgress: LiveData<Boolean> = _inProgress

    protected val _showSuccessMessage = MutableLiveData<String>()
    val showSuccessMessage: LiveData<String> = _showSuccessMessage

    protected val _showErrorMessage = MutableLiveData<String>()
    val showErrorMessage: LiveData<String>
        get() = _showErrorMessage

    abstract fun hydrate()
}
