package com.lepaya.myapplication.extention.toast

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.lepaya.myapplication.R

fun Toast.showSuccessMessage(context: Context, message: String) {
    this.view = inflateToastView(context, message, ContextCompat.getColor(context, R.color.green))
    this.duration = Toast.LENGTH_LONG
    this.setGravity(Gravity.TOP, 0, 200)
    this.show()
}

fun Toast.showErrorMessage(context: Context, message: String) {
    this.view = inflateToastView(context, message, ContextCompat.getColor(context, R.color.red))
    this.duration = Toast.LENGTH_LONG
    this.setGravity(Gravity.TOP, 0, 200)
    this.show()
}

fun inflateToastView(context: Context, message: String, backgroundColor: Int): View {
    val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    val view = inflater.inflate(R.layout.layout_custom_toaster, null)
    val toastMessage = view.findViewById<TextView>(R.id.tvToastMessage)
    val toastLayout = view.findViewById<LinearLayout>(R.id.toastLayout)
    toastLayout.setBackgroundColor(backgroundColor)
    toastMessage.text = message
    return view
}
