package com.lepaya.myapplication.extention.imageview
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.lepaya.myapplication.R
import com.lepaya.myapplication.app.GlideApp

@BindingAdapter(value = ["imageUrl", "skipMemoryCache"], requireAll = false)
fun loadImage(view: ImageView, imageUrl: Any, skipMemoryCache: Boolean = false) {
    GlideApp.with(view.context)
            .load(imageUrl)
            .placeholder(R.color.black)
            .error(R.color.black)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(skipMemoryCache)
            .into(view)
}
