package com.lepaya.myapplication.location

import com.google.android.gms.maps.model.LatLng

data class GeoLocation(val latitude: Double = 0.0, val longitude: Double = 0.0) {
    fun isNotEmpty(): Boolean {
        return !(latitude == 0.0 && longitude == 0.0)
    }

    fun toLatLng(): LatLng {
        return LatLng(latitude, longitude)
    }

    fun getLatLngCompined(): String {
        return "$latitude,$longitude"
    }
}