package com.lepaya.myapplication.location

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.IntentSender
import android.location.Location
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class LastKnownLocationRetrieverImpl(val context: Context) : BaseLocationTracker(context),
    LocationRetriever {

    private val checkLocationSettingsRequestCode = 1002
    private val locationRequest: LocationRequest = createLocationRequest()
    private lateinit var locationCallback: LocationCallback
    private var settingsClient: SettingsClient = LocationServices.getSettingsClient(context)
    private var locationSettingTask: Task<LocationSettingsResponse> =
        settingsClient.checkLocationSettings(getLocationSettingRequest())

    init {
        createLocationRequest()
        checkLocationSettingProvided()
        createLocationCallback()
    }

    private fun getLocationSettingRequest() =
        LocationSettingsRequest.Builder().addLocationRequest(locationRequest).build()

    override fun locationCallback(): LocationCallback = locationCallback

    private fun createLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                locationResult?.lastLocation?.apply {
                    stopLocationUpdates()
                }
            }
        }
    }

    override fun handleActivityResult(requestCode: Int, resultCode: Int) {
        if (requestCode == requestCode() && resultCode == Activity.RESULT_OK) {
            startLocationTracking()
        }
    }

    private fun checkLocationSettingProvided() {
        locationSettingTask.addOnSuccessListener {
            startLocationTracking()
        }

        locationSettingTask.addOnFailureListener { exception ->
            if (exception is ResolvableApiException)
                try {
                    if (context is Activity) {
                        Timber.d("Is Activity Instance of")
                        exception.startResolutionForResult(
                            context as Activity?,
                            checkLocationSettingsRequestCode
                        )
                    } else {
                        Timber.d("Is Not Activity")
                    }
                } catch (exception: IntentSender.SendIntentException) {
                    Timber.e(exception)
                } catch (exception: Exception) {
                    Timber.e(exception)
                }
        }
    }

    @SuppressLint("MissingPermission")
    override suspend fun getLastLocation(): GeoLocation = suspendCoroutine { cont ->
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            if (location != null) {
                Timber.w("Location Return Successfully")
                cont.resume(GeoLocation(location.latitude, location.longitude))
            } else {
                Timber.w("Location Return with Null")
                locationSettingTask = settingsClient.checkLocationSettings(getLocationSettingRequest())
                checkLocationSettingProvided()
                val invalidLocationException = InValidLocationException()
                Timber.e("Invalid location Exception $invalidLocationException")
                cont.resumeWithException(invalidLocationException)
            }
        }
    }

    fun requestCode(): Int = checkLocationSettingsRequestCode
}

data class InValidLocationException(override val message: String = "Location Return with Null") : Exception()