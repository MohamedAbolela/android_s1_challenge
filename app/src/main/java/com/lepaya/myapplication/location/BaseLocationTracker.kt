package com.lepaya.myapplication.location

import android.annotation.SuppressLint
import android.content.Context
import android.os.Looper
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices

abstract class BaseLocationTracker(private val context: Context): BaseLocationRetriever {

    var fusedLocationClient: FusedLocationProviderClient
    private var locationRequest: LocationRequest

    init {
        fusedLocationClient = createFusedLocationClient()
        locationRequest = createLocationRequest()
    }

    fun createLocationRequest(): LocationRequest = LocationRequest.create().apply {
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    abstract fun locationCallback(): LocationCallback

    private fun createFusedLocationClient(): FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

    @SuppressLint("MissingPermission")
    fun startLocationTracking() {
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback(), Looper.myLooper())
    }

    override fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback())
    }
}
