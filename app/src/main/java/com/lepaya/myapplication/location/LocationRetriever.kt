package com.lepaya.myapplication.location

interface BaseLocationRetriever {
    fun stopLocationUpdates()
}

interface LocationRetriever : BaseLocationRetriever {
    suspend fun getLastLocation(): GeoLocation
    fun handleActivityResult(requestCode: Int, resultCode: Int)
}
