package com.lepaya.myapplication.app
import android.app.Application
import com.lepaya.myapplication.BuildConfig
import timber.log.Timber
import timber.log.Timber.DebugTree

class AppTemplate : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(DebugTree())
    }
}
