package com.lepaya.myapplication.business.nearbyFoodVenues

import com.lepaya.myapplication.location.GeoLocation

data class NearbyFoodVenuesViewState(
    val nearbyFoodVenuesList: List<FoodVenuesModel> = emptyList(),
    val userLocation: GeoLocation = GeoLocation(),
    val errorMessage: String = "",
    val error: Throwable? = null
)
