package com.lepaya.myapplication.business.common

import android.app.AlertDialog
import android.content.Context
import android.view.View
import com.lepaya.myapplication.R

class ProgressDialog(val context: Context) {
    private var progressDialog: AlertDialog? = null
    fun showProgressDialog() {
        val builder = AlertDialog.Builder(context)
        val dialogView = View.inflate(context, R.layout.dialog_progress, null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        progressDialog = builder.create()
        progressDialog?.apply {
            show()
        }
    }

    fun hideProgressDialog() {
        progressDialog?.apply {
            if (isShowing)
                dismiss()
        }
    }
}
