package com.lepaya.myapplication.business.nearbyFoodVenues

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lepaya.myapplication.location.LocationRetriever

class NearbyFoodVenuesViewModelFactory(
    private val trendingGithubReposRepository: NearbyVenuesRepository,
    private val locationReceiverService: LocationRetriever
) :
    ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NearbyFoodVenuesVM::class.java)) {
            return NearbyFoodVenuesVM(
                trendingGithubReposRepository,
                locationReceiverService
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
