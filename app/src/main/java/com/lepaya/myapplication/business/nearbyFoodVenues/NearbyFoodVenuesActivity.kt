package com.lepaya.myapplication.business.nearbyFoodVenues

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.lepaya.myapplication.R
import com.lepaya.myapplication.base.BaseActivity
import com.lepaya.myapplication.databinding.ActivityNerbeyFoodVenuesBinding
import com.lepaya.myapplication.databinding.BottomPanelListBinding
import com.lepaya.myapplication.extention.toast.showErrorMessage
import com.lepaya.myapplication.location.LastKnownLocationRetrieverImpl
import com.lepaya.myapplication.source.remote.repoimpl.NearbyFoodVenousRepositoryImpl
import kotlinx.android.synthetic.main.activity_nerbey_food_venues.*
import kotlinx.android.synthetic.main.bottom_panel_list.*
import timber.log.Timber


class NearbyFoodVenuesActivity :
    BaseActivity<NearbyFoodVenuesVM, ActivityNerbeyFoodVenuesBinding>(), OnMapReadyCallback {

    private lateinit var listAdapter: TrendingAdapter
    private lateinit var locationReceiverService: LastKnownLocationRetrieverImpl
    private lateinit var mMap: GoogleMap
    private val REQUEST_CODE = 5005

    private lateinit var sheetBehavior: BottomSheetBehavior<ConstraintLayout>

    override fun getLayoutId(): Int {
        return R.layout.activity_nerbey_food_venues
    }

    override fun initialize() {
        // init dependence
        locationReceiverService = LastKnownLocationRetrieverImpl(context = this)

        // init Viewmodel
        val viewModelFactory = NearbyFoodVenuesViewModelFactory(
            trendingGithubReposRepository = NearbyFoodVenousRepositoryImpl(),
            locationReceiverService = locationReceiverService
        )
        presentationModel =
            ViewModelProvider(this, viewModelFactory).get(NearbyFoodVenuesVM::class.java)

        // bind lifecycle
        dataBindingView.viewmodel = presentationModel

        // bind lifecycle
        dataBindingView.lifecycleOwner = this

        // Render View Model State Observer
        presentationModel.viewState.observe(this, { renderViewModelState(it) })

        // setup recycler adapter
        setupListAdapter()

        //Setup Map
        setupMap()

        // Check app permission
        checkAppPermission()
    }

    private fun renderViewModelState(viewModelState: NearbyFoodVenuesViewState) {
        Timber.e("View State $viewModelState")
        renderErrorMessage(viewModelState)
        renderUpdatedList(viewModelState)
        renderUserLocationOnMap(viewModelState)
    }

    private fun renderUpdatedList(viewModelState: NearbyFoodVenuesViewState) {
        val isEmpty = viewModelState.nearbyFoodVenuesList.isNotEmpty()
        if (isEmpty) {
            listAdapter.submitList(viewModelState.nearbyFoodVenuesList)
            listAdapter.notifyDataSetChanged()
        }
    }

    private fun renderErrorMessage(viewModelState: NearbyFoodVenuesViewState) {
        if (viewModelState.errorMessage.isNotEmpty()) {
            Toast(applicationContext).showErrorMessage(
                applicationContext,
                viewModelState.errorMessage
            )
        }
    }

    private fun setupListAdapter() {
        Timber.w("Setup List View For Trending repos")
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_recycler.layoutManager = layoutManager
        listAdapter = TrendingAdapter {
            presentationModel.getCustomerLocationAndNearbyFoodVenous()
        }
        rv_recycler.adapter = listAdapter
    }

    private fun renderUserLocationOnMap(viewModelState: NearbyFoodVenuesViewState) {
        if (viewModelState.userLocation.isNotEmpty()) {
            val newLatLng = viewModelState.userLocation.toLatLng()
            mMap.addMarker(MarkerOptions().position(newLatLng).title("User Location"))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newLatLng, 15F))
        }
    }

    private fun checkAppPermission() {
        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION,
            ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION,
            ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION,
            ) == PackageManager.PERMISSION_GRANTED -> {
                presentationModel.getCustomerLocationAndNearbyFoodVenous()
            }
            else -> {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ),
                    REQUEST_CODE
                )
            }
        }
    }

    private fun setupMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        sheetBehavior = from(bottom_sheet)


        sheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    STATE_EXPANDED -> "Close Persistent Bottom Sheet"
                    STATE_COLLAPSED -> "Open Persistent Bottom Sheet"
                    else -> "Persistent Bottom Sheet"
                }
            }
        })

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presentationModel.handleLocationActivityResult(requestCode, resultCode)
    }

    override fun onPause() {
        super.onPause()
        presentationModel.stopLocationUpdates()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)
                ) {
                    presentationModel.getCustomerLocationAndNearbyFoodVenous()
                } else {
                    showPermissionDialog()
                }
                return
            }
        }
    }

    private fun showPermissionDialog(){
        AlertDialog.Builder(this@NearbyFoodVenuesActivity)
                .setTitle("Title")
                .setMessage("you can not use app without location please open location")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes) { _, _ ->
                    checkAppPermission()
                }
                .setNegativeButton(android.R.string.no) { _, _ -> showPermissionDialog()}.show()
    }
}
