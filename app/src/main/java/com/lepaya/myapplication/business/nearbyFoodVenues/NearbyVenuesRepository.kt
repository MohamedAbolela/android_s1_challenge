package com.lepaya.myapplication.business.nearbyFoodVenues

import com.mahmoudibra.mvvmtemplate.source.remote.Repository
import com.mahmoudibra.mvvmtemplate.source.remote.Result

interface NearbyVenuesRepository : Repository {
    suspend fun fetchRepositories(latLng: String): Result<List<FoodVenuesModel>>
}
