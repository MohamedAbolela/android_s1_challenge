package com.lepaya.myapplication.business.nearbyFoodVenues

data class FoodVenuesModel(

    var meta: Meta,
    var response: Response
)

data class Response(
    var venues: List<Venues>
)

data class Venues(
    val id: String,
    val name: String,
    var location: Location,
    var categories: List<Categories>,
    var venuePage: VenuePage
)

data class VenuePage(
    val id: String
)

data class Categories(
    val id: String,
    val name: String,
    val pluralName: String,
    val shortName: String,
    var icon: Icon,
    var primary: Boolean
)

data class Icon(
    val prefix: String,
    val suffix: String
)

data class Location(
    val address: String,
    val crossStreet: String,
    val lat: Double,
    val lng: Double,
    var labeledLatLngs: List<LabeledLatLngs>,
    var distance: Int,
    val postalCode: String,
    val cc: String,
    val city: String,
    val state: String,
    val country: String,
    var formattedAddress: List<String>
)

data class LabeledLatLngs(
    val label: String,
    val lat: Double,
    val lng: Double
)

data class Meta(
    var code: Int,
    val requestId: String
)
