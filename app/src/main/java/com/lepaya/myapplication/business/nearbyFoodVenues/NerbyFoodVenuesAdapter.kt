package com.lepaya.myapplication.business.nearbyFoodVenues

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lepaya.myapplication.databinding.ItemFoodVenueBinding

class TrendingAdapter(private val chooseTrendingRepo: (index: Int) -> Unit) :
    ListAdapter<FoodVenuesModel, TrendingAdapter.TrendingRepoViewHolder>(TrendingRepoCallback()) {

    override fun onBindViewHolder(holder: TrendingRepoViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, position, chooseTrendingRepo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrendingRepoViewHolder {
        return TrendingRepoViewHolder.from(parent)
    }

    class TrendingRepoViewHolder private constructor(private val binding: ItemFoodVenueBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: FoodVenuesModel, position: Int, chooseTrendingRepo: (Int) -> Unit) {
            binding.item = item.response.venues[position]
            binding.executePendingBindings()
            binding.layoutContent.setOnClickListener {
                chooseTrendingRepo.invoke(position)
            }
        }
        companion object {
            fun from(parent: ViewGroup): TrendingRepoViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemFoodVenueBinding.inflate(layoutInflater, parent, false)
                return TrendingRepoViewHolder(binding)
            }
        }
    }
}

class TrendingRepoCallback : DiffUtil.ItemCallback<FoodVenuesModel>() {
    override fun areItemsTheSame(oldItem: FoodVenuesModel, newItem: FoodVenuesModel): Boolean {
        return oldItem.response == newItem.response
    }

    override fun areContentsTheSame(
        oldItem: FoodVenuesModel,
        newItem: FoodVenuesModel
    ): Boolean {
        return oldItem.response == newItem.response
    }
}
