package com.lepaya.myapplication.business.nearbyFoodVenues

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.lepaya.myapplication.base.BaseVM
import com.lepaya.myapplication.location.LocationRetriever
import com.mahmoudibra.mvvmtemplate.source.remote.Result
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import timber.log.Timber

class NearbyFoodVenuesVM(
    private val nearbyVenuesRepository: NearbyVenuesRepository,
    private val locationRetriever: LocationRetriever
) : BaseVM() {

    private val _viewStateLD =
        MutableLiveData<NearbyFoodVenuesViewState>().apply {
            value = NearbyFoodVenuesViewState()
        }

    val viewState: LiveData<NearbyFoodVenuesViewState>
        get() = _viewStateLD

    override fun hydrate() {

    }

    fun getCustomerLocationAndNearbyFoodVenous() =
        viewModelScope.launch(CoroutineExceptionHandler { _, thrownException ->
            Timber.w("Exception while fetching nearby food venous  $thrownException")
            _inProgress.value = false
            _viewStateLD.value = _viewStateLD.value?.copy(
              error = thrownException
            )
        }) {
            _inProgress.value = true
            getCustomerLocation()
            loadNearbyFoodVenousRepos()
            _inProgress.value = false
        }


    private suspend fun getCustomerLocation() {
        val userLocation = locationRetriever.getLastLocation()
        _viewStateLD.value = _viewStateLD.value?.copy(
            userLocation = userLocation
        )
    }

    private suspend fun loadNearbyFoodVenousRepos() {
        Timber.w("User Location to load nearby venous ${_viewStateLD.value?.userLocation}")

            _viewStateLD.value = _viewStateLD.value?.copy()
            val response =
                _viewStateLD.value?.userLocation?.getLatLngCompined()?.let {
                    nearbyVenuesRepository.fetchRepositories(
                        it
                    )
                }
            when (response) {
                is Result.Success -> {
                    _viewStateLD.value = _viewStateLD.value?.copy(
                        nearbyFoodVenuesList = response.data
                    )
                    Timber.d("Data is ${response.data}")
                }
                is Result.SystemError -> {
                    _showErrorMessage.value = response.error.meta.errorDetail
                    _viewStateLD.value = _viewStateLD.value?.copy(
                        errorMessage = response.error.meta.errorDetail
                    )
                    Timber.e("Error Message is : ${response.error.meta.errorDetail}")
                }
                is Result.Error -> {
                    _showErrorMessage.value = response.exception.message
                    _viewStateLD.value = _viewStateLD.value?.copy(
                        errorMessage = response.exception.message ?: ""
                    )
                    Timber.e("Network Exception is : ${response.exception}")
                }
            }

    }

    fun handleLocationActivityResult(requestCode: Int, resultCode: Int) {
        locationRetriever.handleActivityResult(requestCode, resultCode)
    }

    fun stopLocationUpdates() {
        locationRetriever.stopLocationUpdates()
    }
}
